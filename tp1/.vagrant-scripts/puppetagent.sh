#!/bin/bash

## Vamos a descargar el paquete que nos permitira instalar el repositorio de puppet
wget -O repositorio-puppet7.deb https://apt.puppetlabs.com/puppet7-release-$(cat /etc/os-release | grep VERSION_CODENAME | cut -d= -f2).deb

## Una vez descargado, necesitamos agregar el repositorio
sudo dpkg -i repositorio-puppet7.deb

## Y actualizamos nuestra lista de repositorios
sudo apt update

## Luego procedemos a instalar puppet
sudo apt install -y puppet-agent

## Configuramos el agente
echo '''[main]
certname = puppet-agent
server = puppet-master
environment = production
runinterval = 15m''' | sudo tee -a /etc/puppetlabs/puppet/puppet.conf

## Habilitamos el servicio
sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

## Sincronizamos con el puppetserver
sudo /opt/puppetlabs/bin/puppet agent -t --server puppet-master