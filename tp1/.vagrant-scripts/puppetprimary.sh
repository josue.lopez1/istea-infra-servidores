#!/bin/sh

### Deshabilitamos el trafico entrante
sudo ufw default deny incoming
### Habilitamos el trafico saliente
sudo ufw default allow outgoing

### Habilitamos el trafico entrante de ssh
sudo ufw allow ssh
### Habilitamos el trafico entrante de http
sudo ufw allow http
### Habilitamos el trafico entrante de https
sudo ufw allow https
### Habilitamos el trafico entrante de 8140
sudo ufw allow 8140
### Habilitamos el trafico entrante de 8142
sudo ufw allow 8142
### Habilitamos el trafico entrante de 8170
sudo ufw allow 8170

## Habilitamos el ufw
sudo ufw --force enable

## Instalamos lvm
sudo apt update && sudo apt -y install lvm2

## Creamos nuestro volumen fisico
sudo pvcreate /dev/sdc
## Creamos nuestro grupo de volumenes
sudo vgcreate puppet /dev/sdc
## Creamos un volumen lógico. En este caso solo le daremos 5GB de los 10GB que tiene el disco. También le vamos a poner de nombre **puppet-master**
sudo lvcreate puppet -L 5G -n puppet-master
## Aplicamos un filesystem al volumen creado anteriormente
sudo mkfs.ext4 /dev/puppet/puppet-master
## Crearemos el directorio /opt/puppetlab/
sudo mkdir -p /opt/puppetlabs
## Montamos nuestro lvm en el directorio creado anteriormente
sudo mount /dev/puppet/puppet-master /opt/puppetlabs
## Agregamos nuestro montaje al fstab
echo "UUID=$(lsblk -f | grep puppet | awk {'print $3'}) /opt/puppetlab ext4 defaults 0 0" | sudo tee -a /etc/fstab


## Vamos a descargar el paquete que nos permitira instalar el repositorio de puppet
wget -O repositorio-puppet7.deb https://apt.puppetlabs.com/puppet7-release-$(cat /etc/os-release | grep VERSION_CODENAME | cut -d= -f2).deb

## Una vez descargado, necesitamos agregar el repositorio
sudo dpkg -i repositorio-puppet7.deb
## Y actualizamos nuestra lista de repositorios
sudo apt update
## Luego procedemos a instalar puppet
sudo apt install -y puppetserver

## Debido a que se levanto una VM con 1GB memoria, vamos modificar la cantidad de memoria que usara la JVM de puppet. En este caso le vamos a dejar solo 512m.
sudo sed -i 's|JAVA_ARGS="-Xms2g -Xmx2g -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger|JAVA_ARGS="-Xms512m -Xmx512m -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger|g' /etc/default/puppetserver

## Vamos a configurar puppet para que el codedir vaya al disco montado
## Creamos el directorio 
# 
# * /opt/puppetlabs/code
#
sudo mkdir -p /opt/puppetlabs/code

## Luego vamos a configurar nuestro puppet.conf con la siguiente configuracion

echo '''# This file can be used to override the default puppet settings.
# See the following links for more details on what settings are available:
# - https://puppet.com/docs/puppet/latest config_important_settings.html
# - https://puppet.com/docs/puppet/latest/config_about_settings.html
# - https://puppet.com/docs/puppet/latest/config_file_main.html     
# - https://puppet.com/docs/puppet/latest/configuration.html

[master]
vardir = /opt/puppetlabs/server/data/puppetserver
rundir = /var/run/puppetlabs/puppetserver
pidfile = /var/run/puppetlabs/puppetserver/puppetserver.pid     
codedir = /opt/puppetlabs/code
dns_alt_names = puppet-master

[main]
certname = puppet-master
server = puppet-master
environment = production
runinterval = 15m

## This is only for testing
[server]
autosign = true''' | sudo tee /etc/puppetlabs/puppet/puppet.conf

## Modificamos donde estara la carpeta code
sudo sed -i 's|server-code-dir: /etc/puppetlabs/code|server-code-dir: /opt/puppetlabs/code|g' /etc/puppetlabs/puppetserver/conf.d/puppetserver.conf

## Movemos los archivos de /etc/puppetlabs/code a /opt/puppetlabs/code
sudo mv /etc/puppetlabs/code /opt/puppetlabs/code
## Añadimos un archivo para hacer test del agente
mkdir -p /opt/puppetlabs/code/environments/production/manifests/

## Creamos un manifiesto para crear el directorio HolaMundo en el path /home
echo '''node /default/ {

    file { "/home/HolaMundo": # Recurso tipo file 

            ensure => "directory", # crea el directorio

            owner => "root", # Owner

            group => "root", # grupo

            mode => "0755", # permisos de diretorio
         }
}
''' | sudo tee -a /opt/puppetlabs/code/environments/production/manifests/site.pp

## Creamos la firma que usara el servidor de puppet
sudo /opt/puppetlabs/bin/puppetserver ca setup

## Levantamos el servicio de puppet y lo habilitamos en el inicio
sudo systemctl start puppetserver && sudo systemctl enable puppetserver
